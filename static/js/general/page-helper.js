activeElement = null;

document.onkeydown = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        deselect();
    }
};

function deselect() {
    if (activeElement && !activeElement.classList.contains('hidden')) {
        activeElement.classList.add("hidden");
        activeElement = null;
        if (debug)
            console.log('Deselect function fired! Forgotten to stopEvent()?');
    }
}

function activate(element) {
    deselect();
    if (element && element.classList.contains('hidden')) {
        element.classList.remove("hidden");
        activeElement = element;
    }
}

function activateById(id) {
    window.event.cancelBubble = true;
    activate(document.getElementById(id));
}

function stopEvent() {
    window.event.cancelBubble = true;
}

function findParentByNode(child, node) {
    ele = child.parentElement;
    while (ele.nodeName != node) {
        ele = ele.parentElement;
        if (!ele)
            return null;
    }
    return ele;
}

function findParentByClass(child, cls) {
    ele = child.parentElement;
    while (!ele.classList.contains(cls)) {
        ele = ele.parentElement;
        if (!ele)
            return null;
    }
    return ele;
}