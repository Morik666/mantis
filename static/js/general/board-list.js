var currentBoardID = null;

function loadBoardList() {
    var scripts = document.getElementsByTagName('script');
    var i = scripts.length, flag = false;
    while (i--) {
        if (scripts[i].src === static_finance_board_js) {
            flag = true;
            break;
        }
    }

    if (!flag) {
        var tag = document.createElement('script');
        tag.src = static_finance_board_js;
        document.getElementsByTagName('head')[0].appendChild(tag);
        var css = document.createElement('link');
        css.rel = 'stylesheet';
        css.href = static_finance_board_css;
        document.getElementsByTagName('head')[0].appendChild(css);
    }

    request = new XMLHttpRequest();
    request.open('GET', url_general_board, true);
    request.onload = function () {
        document.getElementById('boards').innerHTML = request.responseText;
    }
    request.send();
}

function loadBoard(boardID) {
    receive(url_general_board + '/' + boardID, 'main-body', function() {
        currentBoardID = boardID;
        loadCategories();
        loadYear();
    });
}