async function receive(url, containerID, after_load, append = false) {
    let responce = await fetch(url);
    if (responce.ok) {
        let text = await responce.text(); 
        if (append)
            document.getElementById(containerID).appendChild(
                document.createRange().createContextualFragment(text));
        else
            document.getElementById(containerID).innerHTML = text;
        console.log(url + ';' + containerID);
        
        if (after_load)
            after_load();
    }
}

function send(caller, url, after_load) {
    //caller.classList.toggle('button--loading');
    request = new XMLHttpRequest();
    request.open('POST', url, true);
    frm = findParentByNode(caller, "FORM");
    data = {}
    inputs = frm.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].name == 'csrfmiddlewaretoken')
            request.setRequestHeader("X-CSRFToken", inputs[i].value);
        else
            data[inputs[i].name] = inputs[i].value;
    }

    selects = frm.getElementsByTagName('select');
    for (var i = 0; i < selects.length; i++) {
        data[selects[i].name] = selects[i].value;
    }

    //console.log(data);
    request.setRequestHeader("Accept", "application/json");
    request.setRequestHeader("Content-Type", "application/json");
    request.setRequestHeader("HTTP_X_REQUESTED_WITH", "XMLHttpRequest");
    request.onload = function () {
        //caller.classList.toggle('button--loading');
        //console.log(request.responseText);
        if (/*request.readyState === 4 && */request.status === 200) {
            deselect();
        } else {
            frm.getElementsById('message').value = request.message;
            frm.getElementsById('message').classList.toggle('hidden');
        }

        if (after_load)
            after_load();
    }
    request.send(JSON.stringify(data));
}