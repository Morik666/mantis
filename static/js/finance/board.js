var highligted = null;

function loadCategories() {
    receive(url_finance_category + '?boardID=' + currentBoardID, 'categories-container');
}

function openCategoryForm(caller, id, caption) {
    stopEvent()
    frm = document.getElementById("new-category");
    document.getElementById("board_id").value = currentBoardID;
    document.getElementById("category_id").value = id;
    document.getElementById("caption").value = caption;

    frm.style.top = (caller.offsetTop) + 'px';
    frm.style.left = (caller.offsetLeft) + 'px';

    activate(frm);
}

var min_year = 2022;
function loadYear(year = null) {
    if (year == null)
        receive(url_finance_year + '?boardID=' + currentBoardID + '&year=' + min_year, 'years-container', null, true);
}
    
function tableHover(event) {
    categories = document.getElementById("categories-column").childNodes;
    categories.forEach(function (cat) {
        if (cat.offsetTop < event.y && event.y < cat.offsetTop + cat.offsetHeight)
            if (highligted == null) {
                highligted = cat;
                cat.classList.toggle("highligted");
            } else if (highligted == cat) {
                return;
            } else {
                highligted.classList.toggle("highligted");
                highligted = cat;
                cat.classList.toggle("highligted");
            }
            return;
    })
}

function tableLeave() {
    if (highligted != null) {
        highligted.classList.toggle("highligted");
        highligted = null;
    }
}

function expander_hedder_click(div) {
    div.parentElement.parentElement.classList.toggle("expanded-header");
}