import json
from django.http.response import JsonResponse
from django.db.models import Sum
from django.shortcuts import render
from general.datehelp import month_range, day_range
from .models import ExpanceCategory, ExpanceRecord

def __tryint(val: str) -> int:
    try:
        return int(val)
    except:
        return None

def category(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        category = ExpanceCategory(
            id = data['id'] if str(data['id']).isnumeric() else None,
            board_id = data['board_id'],
            caption = data['caption'])
        category.save()
        return JsonResponse({'success': True, 'errorMsg': ''})
    if request.method == 'GET':
        boardID = __tryint(request.GET.get('boardID', ''))
        if not boardID:
            raise NotImplementedError('Not implemented')
        else:
            categories = ExpanceCategory.objects.filter(board_id=boardID).order_by('id')
            res = {'categories': categories}
            return render(request, 'finance/categories.html', res)
    raise NotImplementedError('Not implemented')

def year(request):
    boardID = __tryint(request.GET.get('boardID', ''))
    year = __tryint(request.GET.get('year', ''))
    cat_totals = []
    m_totals = []
    categories = ExpanceCategory.objects.filter(board_id=boardID).order_by('id')
    for category in categories:
        cat_totals.append({'sum': ExpanceRecord.objects.filter(board_id=boardID, category_id=category.id,
            date__gte=f'{year}-1-1', date__lte=f'{year}-12-31').aggregate(Sum('value'))['value__sum'] or 0})
    for month in month_range(year):
        m_cat = []
        for category in categories:
            m_cat.append({'sum': ExpanceRecord.objects.filter(board_id=boardID, category_id=category.id,
                date__gte=month.start, date__lte=month.end).aggregate(Sum('value'))['value__sum'] or 0})
        
        d_totals = []
        for day in day_range(year, month.number):
            d_cat = []
            for category in categories:
                d_cat.append({'sum': ExpanceRecord.objects.filter(board_id=boardID, category_id=category.id,
                    date=f'{year}-{month.number}-{day}').aggregate(Sum('value'))['value__sum'] or 0})
            d_totals.append({'categories': d_cat, 'caption': day})
        
        m_totals.append({'categories': m_cat, 'caption': month.caption, 'days': d_totals})

    res = {'year': year, 'year_categories': cat_totals, 'months': m_totals}
    return render(request, 'finance/year.html', res)
