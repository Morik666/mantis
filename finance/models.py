from django.contrib.auth.models import User
from django.db import models
from general.models import Board

class ExpanceCategory(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    caption = models.CharField(max_length=20)

    def __str__(self):
        return self.caption
    
    def escaped_caption(self):
        return self.caption.replace("'", "\\'")

class ExpanceRecord(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    category = models.ForeignKey(ExpanceCategory, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    value = models.IntegerField()