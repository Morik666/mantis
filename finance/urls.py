from django.urls import path
from finance.apps import FinanceConfig
import finance.views as vw

app_name = FinanceConfig.name
urlpatterns = [
    path('category', vw.category, name='category'),
    path('category/<int:boardID>', vw.category, name='category'),
    path('year', vw.year, name='year'),
]
