# Mantis

My web-app for family managmant.
For now only finance board is working

***

## Authors and acknowledgment
Icons created by: [Freepik on Flaticon](https://www.flaticon.com/authors/freepik)
Planing to purchase custon in future, but this will do for now.

## Progress

## General functionality
 - [ ] Revrite project to use ajax and API to fetch data in seperate batches and process POST requests
 - [ ] Make appropriet color pallet for project
 - [ ] Add ability to shear boards between users
 - [ ] Fix sequrity holes in API
 - [ ] Add ability for new users to register

## Finance board
 - [ ] Accept decimal as input
 - [ ] Accept multiplication (currency)
 - [ ] Implement subcategoris (expandable view just like days in a month)
 - [ ] Highlight day-columns depending on day of week
 - [ ] Highlight expance record, based on user, who made it

## Tasks board
 - [ ] Figure out UX design
 - [ ] Fix board, rewrite in paradime of single moving form
 - [ ] Imlement repeateble tasks

## Other ideas
 - [ ] Notes board
 - [ ] Lists board (movies to watch, books to read, shoping list)
 - [ ] Calendar view
 - [ ] Allow cross-refference between elements on different boards 

***

## ReadMe in pregress

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
For open source projects, say how it is licensed.