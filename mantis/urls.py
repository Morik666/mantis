from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('mantis/', include('general.urls', namespace='general')),
    path('finance/', include('finance.urls', namespace='finance')),
]
