from django.contrib.auth.models import User
from django.urls import reverse
from django.db import models

class Board(models.Model):
    class BoardType(models.IntegerChoices):
        Tasks = 0, 'Tasks'
        Finance = 1, 'Finance'
    
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    caption = models.CharField(max_length=20)
    type = models.IntegerField(choices=BoardType.choices)

    def __str__(self):
        return self.caption
    
    def get_url(self):
        return reverse("general:boardid", args=[str(self.id)])