from django.urls import path
from general.apps import GeneralConfig
import general.views as vw

app_name = GeneralConfig.name
urlpatterns = [
    path('', vw.mainView, name='mainView'),
    path('<int:boardID>', vw.mainView, name='mainView'),
    path('board', vw.board, name='board'),
    path('board/<int:boardID>', vw.board, name='boardid'),
]
