import datetime

class Month:
    __month_names = ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень',
            'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень']

    def __init__(self, year: int, number: int):
        self.year = year
        self.number = number
    
    @property
    def start(self) -> str:
        return f'{self.year}-{self.number}-1'
    
    @property
    def end(self) -> str:
        next_month = datetime.datetime(self.year, self.number, 28) + datetime.timedelta(days=4)
        res = next_month - datetime.timedelta(days=next_month.day)
        return f'{self.year}-{self.number}-{res.day}'

    @property
    def caption(self) -> str:
        return self.__month_names[self.number - 1]

def month_range(year: int):
    for i in range(12, 1, -1):
        diff = (datetime.datetime(year, i, 1) - datetime.datetime.now()).days + 1
        if diff <= 0:
            yield Month(year, i)

def day_range(year: int, month: int):
    for i in range(31, 0, -1):
        try:
            diff = (datetime.datetime(year, month, i) - datetime.datetime.now()).days + 1
        except ValueError:
            continue
        if diff <= 0:
            yield i