from django.http.response import JsonResponse
from django.shortcuts import render
from .models import Board

import json

def mainView(request, boardID=None):
    board_types = Board.BoardType.choices
    #res = {'board_types': board_types, 'debugMode': 1 if settings.DEBUG else 0 }
    res = {'board_types': board_types, 'debugMode': 1}
    return render(request, 'general/base.html', res)

def board(request, boardID=None):
    if request.method == 'POST':
        data = json.loads(request.body)
        board = Board(
            id = data['id'] if str(data['id']).isnumeric() else None,
            caption = data['caption'],
            type = data['type'],
            owner = request.user)
        board.save()
        return JsonResponse({'success': True, 'errorMsg': ''})
    elif request.method == 'GET':
        if not boardID:
            if request.user.is_authenticated:
                all_boards = Board.objects.filter(owner=request.user)
                res = {'boards': all_boards}
            else:
                res = {}
            # if request.user.id == 3:  # TODO: Implement propper board shearing
            #    sheared_board = Board.objects.filter(id = 3)
            #    all_boards = all_boards.union(sheared_board)
            return render(request, 'general/board-list.html', res)
        else:
            board = Board.objects.get(id=boardID)
            if board.type == Board.BoardType.Finance:
                res = {'current_board': board }
                return render(request, 'finance/board.html', res)
            else:
                print(f'type: {board.type}| type"s type: {type(board.type)}')
    return JsonResponse({'success': False, 'errorMsg': 'Something went wrong'})
